<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::get('/method/{method?}.{action?}', 'ApiHandlers@main');
Route::get('/test', function (){
    return response('ok');
});
Route::post('/dialogflow', 'Dialogflow@main');
Route::post('/telegram', 'Telegram@handle');
Route::get('/telegram/set', 'Telegram@set');