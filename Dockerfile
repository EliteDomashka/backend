#based on https://bitbucket.org/EdBoraas/apache-docker/src/master/laravel/Dockerfile
FROM eboraas/apache
MAINTAINER Alexey Lozovyagin <oleksih@gmail.com>

RUN  apt-get update && apt-get -yqq install apt-transport-https lsb-release ca-certificates wget unzip && \
                         wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
                         echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list && \
                         apt-get -qq update && apt-get -qqy upgrade
RUN  apt-get -y install git curl php7.2 php-mysql php-mcrypt php-json php-xml php-mbstring php-curl php-zip && apt-get -y autoremove && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN /usr/sbin/a2enmod rewrite

RUN /usr/bin/curl -sS https://getcomposer.org/installer |/usr/bin/php
RUN /bin/mv composer.phar /usr/bin/composer
RUN chmod +x /usr/bin/composer \
 && mkdir -p /var/www/EliteDomashka/
# && git clone https://gitlab.com/EliteDomashka/backend.git /var/www/EliteDomashka \
ADD --chown=www-data:www-data  . /var/www/EliteDomashka/
WORKDIR /var/www/EliteDomashka
RUN  COMPOSER_CACHE_DIR="/tmp" && composer install \
 && composer clear-cache \
 && rm -rf /var/www/EliteDomashka/.composer /var/cache/apk/*
# && /bin/chown www-data:www-data -R /var/www/EliteDomashka/storage /var/www/EliteDomashkal/bootstrap/cache

ADD config/000-laravel.conf  /etc/apache2/sites-available/000-laravel.conf
RUN /usr/sbin/a2dissite '*' && /usr/sbin/a2ensite 000-laravel
#RUN chown -R www-data:www-data /var/www/EliteDomashka

EXPOSE 80
EXPOSE 443

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
