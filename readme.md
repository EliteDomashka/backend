# EliteDomashka Backend App
Backend часть "Элитной домашки"

## Установка
`docker-compose up`, устанвока docker-compose [тут](https://docs.docker.com/compose/install/)   
Также нужно сдулать импорт таблиц, введите эту комманду при *запущеном* контейнере `docker exec -t api_elitedomashka_1 php artisan ch:scheme`
## TODO List
- [x] Docker   
## Лицензия
Copyright (c) 2018 Alexey Lozovyagin. Code released under the [GNU AGPLv3](https://gitlab.com/EliteDomashka/backend/blob/master/LICENSE)