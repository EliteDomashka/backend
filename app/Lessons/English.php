<?php
/**
 * User: alexey
 * Date: 20.10.18
 * Time: 12:43
 */

namespace App\Lessons;


class English extends Lesson {
    public $templates = [
//        "p.{%two%} ex.{%one%}",
        "p{%two%} ex{%one%}",
        "p {%two%} ex {%one%}"
    ];
    public $example = 'p 91 ex 2';
    public $lesson_id = 4;
    public $reg ="/p[.| ]+([0-9]*)[ ]+[Ex|ex]+.([0-9]*)/";
}