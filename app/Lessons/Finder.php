<?php
/**
 * User: alexey
 * Date: 03.11.18
 * Time: 21:34
 */

namespace App\Lessons;


use App\Http\Controllers\Utils\DBConnect;
use App\TextParser;

class Finder {
    use DBConnect;

    public static function getById(int $id): ?Lesson{
        $lessons = [
            Algebra::class,
            English::class,
            Geometry::class,
            Physics::class,
            UkrLang::class
        ];
        foreach ($lessons as $class){
            if(($lol = new $class)->lesson_id === $id) return $lol;
        }
        return null;
    }
    public static function parse($lesson_id, $task): ?array {
        $task = str_replace([',', '№'], ['.', '#'], $task);
        $task = mb_strtolower($task);
        $result = null;
        $tp = new TextParser();
        $lesson = self::getById($lesson_id);

        if($lesson->reg != null) {
           if (preg_match($lesson->reg, $task)){
               $result = array_reverse(preg_split($lesson->reg, $task, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE));
//               $result = preg_split($lesson->reg, $task, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
           }
        }

        if ($result == null || (is_array($result) && count($result) != 2 )) foreach($lesson->templates as $template){ //TODO: drop
            if(!is_array($result)) $result = $tp->parse($task, $template);

        }

        if(is_array($result) && isset($result['one'])){
            $result2 = [$result['one']];
            if(isset($result['two'])) $result2[1] = $result['two'];
            $result = $result2; unset($result2);
        }

        return $result ?? null;
    }
    public static function getURL($lesson_id, array $task_arr): ?string {
        return self::getDB()->select("SELECT img FROM gdz WHERE lesson_id = {$lesson_id} AND task = ".str_replace('"', "'", json_encode($task_arr)))->fetchOne('img') ?? null;
    }
}