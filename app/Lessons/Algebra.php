<?php
/**
 * User: alexey
 * Date: 21.10.18
 * Time: 16:21
 */

namespace App\Lessons;


class Algebra extends Lesson {
    public $lesson_id = 1;
    public $templates = [
        "#{%one%}",
        "{%one%}"
    ];
    public $example = "4.9";
    public $reg = "/[#№]\b(\d{1,2}[,.]\d{1,2})/";
}