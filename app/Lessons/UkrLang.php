<?php
/**
 * User: alexey
 * Date: 20.10.18
 * Time: 20:26
 */

namespace App\Lessons;


class UkrLang extends English {
    public $lesson_id = 3;
    public $templates = [
        "стр {%two%} впр {%one%}",
        "с {%two%} в {%one%}",
        "стр {%two%} #{%one%}",
        "впр {%one%} стр {%two%}"
    ];
    public $example = 'стр 73 впр 3';
    public $reg = "/[стр]+[.| ]+(\d*)[ ]+[впр]+[.| ]+([\d*])/";
//    public $reg = "/[стр]+[.| ]+(\d*)[ ]+[впр]+[.| ]+([\d*|🏠])/"; //TODO: add 🏠
}