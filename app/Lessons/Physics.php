<?php
/**
 * User: alexey
 * Date: 20.10.18
 * Time: 21:19
 */

namespace App\Lessons;


class Physics extends Lesson {
    public $lesson_id = 8;
    public $templates = [
        "впр.{%one%} #{%two%}",
        "впр.{%one%} # {%two%}",
        "впр {%one%} #{%two%}",
        "впр {%one%} # {%two%}",
    ];
    public $example = 'впр 12 #4';
    public $reg = "/[впр]+[.| ]+([\d]*)[ ]+[#|№]+(\d*)/";
}