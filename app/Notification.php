<?php
/**
 * User: alexey
 * Date: 15.10.18
 * Time: 17:22
 */

namespace App;

use App\Http\Controllers\Utils\DBConnect;

class Notification {
    use DBConnect;
    static $basic ="SELECT id, task_id, week, action FROM notification ANY LEFT JOIN (SELECT * FROM tasks FINAL) USING task_id";
    public static function getNotifyByIDs(array $ids){
        if(count($ids) > 0){
            $str = self::$basic." WHERE";
            $str .= " (week >= ".(date('W')-1)." OR week == 0)";
            $str .= " AND (id == ".array_shift($ids);
            foreach ($ids as $id){
                $str .= " OR id == {$id}";
            }
            $str .= ") ORDER BY action ASC";
            return self::getDB()->select($str)->rawData()['data'];
        }
    }
}