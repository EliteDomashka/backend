<?php
namespace App\Http\Controllers\Utils;

class Week {
    public static $days= [
            1 => "Понеділок",
            2 => "Вівторок",
            3 => "Середа",
            4 => "Четверг",
            5 => "П'ятниця"
        ];
    public static function getDayString($day) {
        return self::$days[$day];
    }

}
