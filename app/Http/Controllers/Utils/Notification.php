<?php
/**
 * User: alexey
 * Date: 17.08.18
 * Time: 18:44
 */

namespace App\Http\Controllers\Utils;

use App\Task;
use paragraph1\phpFCM\Client as FCMClient;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\{Topic, Device};

class Notification {
    use FCMConnect;
    public function __construct($token = null) {
        $this->token = $token;
    }

    public function sendTaskByToken($task_data){
        return $this->sendData('newtask', $task_data,2);
    }
    public function sendTaskById(int $task_id){
        $result = Task::getTaskById($task_id);
        return $this->sendData('newtask', $result);
    }
    public function sendData(string $type, array $data, int $stype = 1, $topic = 'tasks'){
        $message = new Message();
        if($stype == 1) $recipient = new Topic($topic);
        if($stype == 2) $recipient = new Device($this->token);
        $message->addRecipient($recipient);
        $data['type'] = (string)$type;
        $message->setData($data);

        return $this->getConnection()->send($message);
    }
}
trait FCMConnect{
    public function getConnection(){
        if(@$this->fcm === null){
            $client = new FCMClient();
            $client->setApiKey(env('FB_KEY'));
            $client->injectHttpClient(new \GuzzleHttp\Client());
            $this->fcm = $client;
        }
        return $this->fcm;
    }
}