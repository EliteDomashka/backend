<?php
/**
 * User: alexey
 * Date: 17.08.18
 * Time: 11:38
 */

namespace App\Http\Controllers\Utils;


use ClickHouseDB\Client;

trait DBConnect {
    private static $db;
    public static function getDB(): Client {
        if (@self::$db === null) {
            $db = new Client(['host' => env('CH_HOST'), 'port' => env('CH_PORT'), 'username' => env('CH_USERNAME'), 'password' => env('CH_PASSWORD'),], ['https' => env('CH_HTTPS')]);
            $db->database(env('CH_DATABASE'));
            $db->setTimeout(1.5);
            $db->setConnectTimeOut(5);
            self::$db = $db;
        }
        return self::$db;
    }
}