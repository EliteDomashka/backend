<?php
/**
 * User: alexey
 * Date: 17.10.18
 * Time: 19:59
 */

namespace App\Http\Controllers\Utils;

use Carbon\Carbon;

class Util {
    public static function getDayByDayAndWeek($day, $week): string {
        return ($d = Carbon::create(date('Y'), 1, 1, 0,0,0, 'Europe/Kiev')->startOfWeek()->addWeek($week-1)->addDay($day-1))->format('d.m.Y');
    }
    public static function taskText(array $task, $full = false){
        $msg = "";
        $msg .= "№".$task['num'].' '.$task['name'].": ";
        $msg .= $task['task'];
        if($task['description'] != 'null' && $full === false)
            $msg .= '... ';
        elseif ($full === true){
            $msg .= '. '.$task['description'];
        }
        return $msg;
    }
}