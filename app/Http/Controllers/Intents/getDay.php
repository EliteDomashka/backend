<?php
/**
 * User: alexey
 * Date: 17.10.18
 * Time: 7:19
 */

namespace App\Http\Controllers\Intents;

use App\Http\Controllers\Utils\Week;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class getDay extends Intent {
    public $name = "get-day";
    public function run(){
        $request = request();
        $result = $request['queryResult'];
        $params = $result['parameters'];
        $date = Carbon::parse($params['date']);

        $tasks = Task::_getDayByWeekAndDay(($week = $date->weekOfYear), ($day = $date->dayOfWeek));
        $text = "";
        $additional_text = "";
        foreach ($tasks as $task){
            $text.= $this->taskText($task).PHP_EOL;
        }
        switch ($c = count($tasks)) {
            case 0:
                $additional_text = "Ничего не задали";
                break;
            case 1:
            case 2:
                $additional_text = "Задали чучуть";
                break;
            case 3:
            case 4:
                $additional_text = "Задали не много";
                break;
            default:
                $additional_text = "Много задали, всего {$c} предметов";
        break;
        }
        if($text != "") $text = self::getDayByDayAndWeek($day, $week)."  (".Week::getDayString($day).")".PHP_EOL.$text;

        $response['payload']['telegram']['text'] = $text;
        $response['fulfillmentText'] = $text;

        $response['source'] = 'backend-school.in-story.org';
        $response['payload']['google'] = [  ];
        $suggestions = [];
        foreach ($tasks as $task){
            if($task['description'] !== 'null') $suggestions[] = ['title' => $task['name']];
        }
        $session = $request['session'];
        $response['outputContexts'] = [
            [
                'name' => $session.'/contexts/'.(new getDay())->name,
                'lifespanCount' => count($suggestions),
                'parameters' => [
                    'week' => $week,
                    'day' => $day,
                    'suggestions'=> $suggestions
                ]
            ]
        ];
        $response['payload']['google'] = [
            'expectUserResponse' => true,
            'richResponse' => [
                'items' => [
                    [
                        'simpleResponse' => [
                            "textToSpeech"=> $additional_text,
                            "displayText"=> $text
                        ],

                    ],
                ],
                'suggestions'=> $suggestions
            ],
        ];
        Log::info($response);

        return response()->json($response);
    }
}