<?php
/**
 * User: alexey
 * Date: 17.10.18
 * Time: 18:54
 */

namespace App\Http\Controllers\Intents\fllowup;

use App\Http\Controllers\Intents\getDay;
use App\Task;
use Illuminate\Support\Facades\Log;

class getLesson extends getDay {
    public $name = "get-day-custom";
    public function run() {
        $request = request();
        $data = $request['queryResult']['outputContexts'];
        $response = [];
        foreach ($data as $row){
            if(@is_numeric($row['parameters']['week'])){
                $data = $row['parameters'];
                break;
            }
        }
        $task = Task::getTaskByNameAndDate($data['lesson'], $data['week'], $data['day']);
        $response['payload']['google'] = [
            'expectUserResponse' => true,
            'richResponse' => [
                'items' => [
                    [
                        'simpleResponse' => [
                            "textToSpeech"=> $task['task'],
                            "displayText"=> $this->taskText($task, true)
                        ],

                    ],
                ]
            ],
        ];
        $suggestions = [];
        foreach ($data['suggestions'] as $suggestion){
            if($suggestion['title'] != $data['lesson']){
                $suggestions[] = $suggestion;
            }
        }
        if(!empty($suggestions)) $response['payload']['google']['richResponse']['suggestions'] = $suggestions;
        Log::warning($response);
        return response()->json($response);
    }
}