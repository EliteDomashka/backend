<?php
/**
 * User: alexey
 * Date: 15.10.18
 * Time: 20:38
 */

namespace App\Http\Controllers;


use App\Http\Controllers\Intents\getDay;
use App\Http\Controllers\Intents\fllowup\getLesson;
use App\Http\Controllers\Notify\Telegram;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Dialogflow extends Controller {
    public $intents = [];
    public function init(){
        $intents = [
            getDay::class,
            getLesson::class
        ];
        foreach ($intents as $intent){
            $intent = new $intent();
            $this->intents[$intent->name] = $intent;
        }
    }
    public function main(){
        $this->init();
//        Log::error(request()['originalDetectIntentRequest']['payload']['user']);
        return $this->intents[request()['queryResult']['intent']['displayName']]->run();
    }
}