<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\Admin;
use App\Http\Controllers\Api\Api;
use App\Http\Controllers\Api\Gdz;
use App\Http\Controllers\Api\Lessons;
use App\Http\Controllers\Api\Task;
use Illuminate\Http\Request;

class ApiHandlers extends Controller {
    /* @type Api[] */
    private $handlers = [];
    public function __construct() {
        $this->registerClass(new Task());
        $this->registerClass(new Admin());
        $this->registerClass(new Lessons());
        $this->registerClass(new Gdz());
    }
    public function main($method, $action = ""){
        if (isset($this->handlers[$method])){
            return $this->handlers[$method]->run($action) ?? response()->json(['error' => true]);
        }
    }
    public function registerClass(Api $class){
        $this->handlers[$class::NAME] = $class;
    }
}
