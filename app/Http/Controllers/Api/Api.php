<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Utils\DBConnect;
use Symfony\Component\HttpFoundation\Response;

abstract class Api {
    use DBConnect;
    const NAME = "";

    abstract public function run(string $action): Response;
    public function send(array $data = []){
        if(empty($data)) $data = ['success' => false];
        if(!isset($data['success']) && isset($data['response'])) $data['success']= true;
        if ($data['success'] == true) unset($data['error']);
        return response()->json($data);
    }
}