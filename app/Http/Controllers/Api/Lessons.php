<?php
/**
 * User: alexey
 * Date: 01.09.18
 * Time: 19:43
 */

namespace App\Http\Controllers\Api;



use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;

class Lessons extends Api {
    public const NAME = 'lessons';

    public function run(string $action): Response {
        $result = ['success' => false];
        $success = &$result['success'];
//        $error = &$result['error'];
        if($action == "getAll"){
            $success = true;
            $result['response'] = \App\Lessons::getAll();
            return $this->send($result);
        }else if($action == 'getLessons'){
            $success = true;
            $result['response'] = \App\Lessons::getLessons();
            return $this->send($result);
        }else if($action == 'getSchedule'){
            $success = true;
            $result['response'] = \App\Lessons::getSchedule(request()->get('day'));
            return $this->send($result);
        }else if($action == 'getNextLesson'){
            if(is_numeric($day = request()->get('day')) && is_numeric($num =request()->get('num'))){
                $success = true;
                $result['response'] = \App\Lessons::findNextLesson($day, $num);
            }
        }
        return $this->send($result);
    }
}