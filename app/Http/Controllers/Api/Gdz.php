<?php
/**
 * User: alexey
 * Date: 03.11.18
 * Time: 21:31
 */

namespace App\Http\Controllers\Api;


use App\Lessons\Finder;
use Symfony\Component\HttpFoundation\Response;

class Gdz extends Api {
    public const NAME = "gdz";

    public function run(string $action): Response {
        $response = [];
        switch ($action) {
            case 'get':
                if (is_numeric($lesson_id = request()->get('lesson_id')) && is_string($task = request()->get('task'))) {
                    $result = Finder::parse($lesson_id, $task);
                    if (is_array($result)) {
                        $url = Finder::getURL($lesson_id, $result);
                        if (!is_null($url)) {
                            return $this->send(['success' => true, 'url' => $url]);
                        }
                    }
                }
                break;
            case 'getRexExps':
                $result = [];
                foreach ([1, 2, 3, 4, 8] as $lesson_id) {
                    $result[$lesson_id] = Finder::getById($lesson_id)->reg;
                }
                $response['response'] = $result;
                break;
        }
        return $this->send($response);
    }
}