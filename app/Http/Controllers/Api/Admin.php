<?php
/**
 * User: alexey
 * Date: 17.08.18
 * Time: 12:39
 *
 * Всё что делает редактор тут, и запись только тут
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Notify\Pusher;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;

class Admin extends Api {
    public const NAME = "admin";
    public function run(string $action): Response {
        $result = [
            'success' => false
        ];
        $success = &$result['success'];
        $error = &$result['error'];
        switch ($action){
            case 'addTask':
                if (!is_null(($date = request()->get('date')))){
                    $dt = Carbon::parse($date);
                    if (($lesson_num = request()->get('lesson_num')) <= 9 && $lesson_num > 0){
                            if(strlen(($task = request('task'))) < 3){
                                $error = "По подробней пожалуйста";
                                return $this->send($result);
                            }

                            $task_id = $this->getDB()->select('SELECT task_id FROM tasks ORDER BY task_id desc LIMIT 1')->fetchOne('task_id')+1;
                            $description = request()->get('description');
                            if(is_null($description)) $description = "null";

                            $this->getDB()->insert('tasks',
                                [
                                    ($task = [$dt->weekOfYear, (int)$dt->dayOfWeek, (int)$lesson_num, $task, (int)$task_id, $description]),
                                ],
                                ['week', 'day', 'lesson_num', 'task', 'task_id', 'description']
                            );
                            Pusher::pushTaskToAll($task_id, 'newtask');
                            $success = true;
                    }else $error = "lesson_num должен быть <= 9 и > 0";
                }
                break;
            case 'removeTask':
                if(is_numeric(($task_id = request()->get('task_id')))){
                    $this->getDB()->insert('tasks',
                        [
                            ($task = [(int)date('W'), (int)1, (int)1, " ", (int)$task_id, -1]),
                        ],
                        ['week', 'day', 'lesson_num', 'task', 'task_id', 'version']
                    );
                    Pusher::pushToAll('deltask', ['task_id' => (int)$task_id]);
                    $success = true;
                }

                    break;
            case 'editTask':
                $task = json_decode(request()->get('task'), true);
                if(!empty($task)){
                    $task['lesson_num'] = (int)$task['num'];
                    unset($task['num']);
                    unset($task['name']);
                    unset($task['lid']);
                    $task['task_id'] = (int)$task['task_id'];
                    $task['week'] = (int)$task['week'];
                    $task['day'] = (int)$task['day'];
                    $this->getDB()->insert('tasks',
                        [
                            array_values($task),
                        ],
                        array_keys($task)
                    );
                    Pusher::pushTaskToAll($task['task_id'], 'edittask');
                    $success = true;
                }
                break;
            case 'pushDay':
                if(is_numeric(( $day = request()->get('day')))){
                    $items = request()->get('items');
                    $keys = null;
                    $num = 1;
                    if(!empty($items)) foreach ($items as $i => &$item){
                        $item = json_decode($item, true);
                        if (is_array($item)){
                            if ($keys == null){
                                $keys = array_keys($item);
                            }
                            $item['num'] = $num++;
                            $item = array_values($item);
                            $item[2] = (int)$item[2];
                        }else{
                            unset($items[$i]);
                        }
                    }

                    $this->getDB()->insert('lessons',
                        $items,
                        $keys
                    );

                }else $error = "day not numeric";
                break;
            case 'removeLesson':
                if(is_numeric(($num = request()->get('num')))){
                    if(is_numeric(($day = request()->get('day')))){
                        $this->getDB()->insert('lessons', [
                            [(int)$day, (int)$num, -1]
                        ], ['day', 'num', 'version']);
                        $success = true;
                    }
                }
                break;
        }
        return $this->send($result);
    }
}