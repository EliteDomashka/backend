<?php
/**
 * User: alexey
 * Date: 17.08.18
 * Time: 11:33
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Notify\FCM;
use App\Lessons\Finder;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;

class Task extends Api {
    public const NAME = 'task';

    public function run(string $action): Response {
        $response = ['success' => false];
        $success = &$response['success'];
        $error = &$response['error'];
        switch ($action) {
            case 'sync':
                if (is_string($token = request()->get('token'))) {
                    $tasks = \App\Task::getAll();
                    Log::warning('sync ' . $token);
                    foreach ($tasks as $task) {
                        (new FCM($token))->sendTaskByToken($task);
                    }
                    (new FCM($token))->sendData('endsync', ['ok' => true], 2);
                    $success = true;
                } else {
                    $error = 'Token not found';
                }
                break;
            case 'smartSync':
                if (is_string($token = request()->get('token')) && is_array($missing = request()->get('missing'))) {
                    Log::warning('smartSync ' . $token);
                    $notifications = \App\Notification::getNotifyByIDs($missing);
                    $fcm = new FCM($token);
                    Log::error($notifications);
                    Log::error($missing);
                    foreach (@$notifications as $notify) {
                        $fcm->sendData($notify['action'], \App\Task::getTaskById($notify['task_id']), 2); // getTaskById вопхать в первый запрос
                    }
                    $fcm->sendData('endsync', ['ok' => true], 2);
                    $success = true;
                } else {
                    $error = 'Token not found';
                }
                break;
            case 'getWeek':
                $result = \App\Task::getAllByWeek(request()->get('week'));
                if (!empty($result)) {
                    $success = true;
                    $response['response'] = $result;
                }
                break;
        }
        return $this->send($response);
    }
}