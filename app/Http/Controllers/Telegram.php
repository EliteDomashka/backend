<?php
/**
 * User: alexey
 * Date: 29.10.18
 * Time: 17:11
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Log;
use PhpTelegramBot\Laravel\PhpTelegramBotContract;

class Telegram extends Controller {
    public function handle(PhpTelegramBotContract $bot){
        try {
            $bot->handle();
        } catch (\Longman\TelegramBot\Exception\TelegramException $e) {
            Log::info($e->getMessage());
        }
//        Log::info(app_path('Telegram/Commands'));
    }
    public function set(PhpTelegramBotContract $bot){
        dump($bot->setWebhook('https://backend-school.in-story.org/api/telegram'));
//        $bot->handle();
    }
}