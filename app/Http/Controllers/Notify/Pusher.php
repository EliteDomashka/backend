<?php
/**
 * User: alexey
 * Date: 10.09.18
 * Time: 19:26
 */

namespace App\Http\Controllers\Notify;


use App\Task;

class Pusher {
    public static $services = [
        FCM::class,
        Telegram::class
    ];
    public static function pushTaskToAll(int $task_id, string $action){
        $data = Task::getTaskById($task_id);
        self::pushToAll($action, $data);
    }
    public static function pushToAll(string $action, array $data){
        foreach (self::$services as $service){
            $lol = new $service();
            $lol->emit($action, $data);

        }
    }
}