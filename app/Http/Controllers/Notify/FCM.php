<?php
/**
 * User: alexey
 * Date: 17.08.18
 * Time: 18:44
 */

namespace App\Http\Controllers\Notify;

use App\Http\Controllers\Utils\DBConnect;
use App\Task;
use Illuminate\Support\Facades\Log;
use paragraph1\phpFCM\Client as FCMClient;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\{Topic, Device};

class FCM extends \App\Http\Controllers\Notify\Notification {
    use FCMConnect, DBConnect;
    public function __construct($token = null) {
        $this->token = $token;
    }

    public function sendTaskByToken($task_data){
        return $this->sendData($this->actionToInt('newtask'), $task_data,2);
    }
    public function emit(string $action, array $data) {
        if (($action = $this->actionToInt($action)) != -1) {
            if (!isset($data['task_id'])) return;
            $id = self::getDB()->select('SELECT id FROM notification FINAL ORDER BY id DESC LIMIT 1')->fetchOne('id') ?? 0;
            $id++;
            Log::alert($id);
            self::getDB()->insert('notification', [[$id, $data['task_id'], $action]], ['id', 'task_id', 'action']);
            $data['notify_id'] = $id;
        }
        $this->sendData($action, $data);
    }

    public function sendTaskById(int $task_id){
        $result = Task::getTaskById($task_id);
        return $this->sendData('newtask', $result);
    }
    public function sendData($type, array $data, int $stype = 1, $topic = 'tasks'){
        $message = new Message();
        if($stype == 1) $recipient = new Topic($topic);
        if($stype == 2) $recipient = new Device($this->token);
        $message->addRecipient($recipient);
        $data['type'] = $type;
        $message->setData($data);
        $result = $this->getConnection()->send($message);
        if($stype == 1) {
            Log::warning('topic: '.$topic);
        }else if($stype == 2){
            Log::warning('device '.$this->token);
        }
        Log::warning($data);
        Log::info($result->getReasonPhrase());
        return $result;
    }
}
trait FCMConnect{
    public function getConnection(){
        if(@$this->fcm === null){
            $client = new FCMClient();
            $client->setApiKey(env('FB_KEY'));
            $client->injectHttpClient(new \GuzzleHttp\Client());
            $this->fcm = $client;
        }
        return $this->fcm;
    }
}