<?php
/**
 * User: alexey
 * Date: 10.09.18
 * Time: 18:24
 */

namespace App\Http\Controllers\Notify;


use App\Http\Controllers\Utils\Util;

abstract class Notification extends Util {
    public function emit(string $action, array $data){}
    public function actionToInt(string $action): ?int {
        $actions = [
          'newtask' => 1,
          'edittask' => 2,
          'deltask' => 3
        ];
        return $actions[$action] ?? -1;
    }
}