<?php
/**
 * User: alexey
 * Date: 10.09.18
 * Time: 19:20
 */

namespace App\Http\Controllers\Notify;


use App\Http\Controllers\Utils\DBConnect;
use App\Http\Controllers\Utils\Week;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Request;
use PhpTelegramBot\Laravel\PhpTelegramBot;

class Telegram extends Notification  {
    use DBConnect;
    /* @type \PhpTelegramBot\Laravel\PhpTelegramBot */
    private $bot;
    public function __construct() {
        $bot = new PhpTelegramBot(env('TG_API'));
        Request::initialize($bot);
        $this->bot = $bot;
    }

    public function emit(string $action, array $data) {
        if($action == 'newtask'){
            $msg_id = Request::sendMessage([
                'chat_id' => env('TASK_CHANNEL'),
                'text' => $this->genText($data),
                'parse_mode' => 'markdown',
                'disable_notification' => true
            ])->getResult()->message_id;
            self::getDB()->insert('tg_msg', [['task_id' => $data['task_id'], 'msg_id' => $msg_id]]); //ключи игоряться
        }elseif ($action == 'edittask'){
            $msg_id = self::getDB()->select("SELECT msg_id FROM tg_msg WHERE task_id = {$data['task_id']} LIMIT 1")->fetchOne('msg_id');
            Request::editMessageText([
                'chat_id' => env('TASK_CHANNEL'),
                'message_id' => $msg_id,
                'text' => $this->genText($data),
                'parse_mode' => 'markdown'
            ]);
        }elseif ($action == "deltask"){
            $msg_id = self::getDB()->select("SELECT msg_id FROM tg_msg WHERE task_id = {$data['task_id']} LIMIT 1")->fetchOne('msg_id');
            Request::deleteMessage([
                'chat_id' => env('TASK_CHANNEL'),
                'message_id' => $msg_id
            ]);
        }
        if(isset($data['day']) && isset($data['week'])){
            $lastDailyPost = self::getDB()->select('SELECT msg_id FROM daily_msg FINAL WHERE day = {0} AND week = {1}', [$data['day'], $data['week']])->fetchOne('msg_id');
            if(is_numeric($lastDailyPost)){
                Artisan::call('daily:send');
            }
        }
    }
    public function dailySend(array $tasks, int $week, int $day){
        $msg = "#week".$week.PHP_EOL;
        $msg .="Вся домашка на ".self::getDayByDayAndWeek($day, $week)."  (".Week::getDayString($day).")".PHP_EOL;
        if (count($tasks) > 0)
        foreach ($tasks as $task){
            $msg_id = self::getDB()->select("SELECT msg_id FROM tg_msg WHERE task_id = {$task['task_id']} LIMIT 1")->fetchOne('msg_id');
            $msg .= $this->genText($task, true);
            if($task['description'] !== 'null'){
                $msg .= "[...](https://t.me/". mb_substr(env('TASK_CHANNEL'), 1)."/{$msg_id}) ";
            }
            $msg .= PHP_EOL;
        }
        else $msg = "Відсутня";
        $last_post = self::getDB()->select('SELECT msg_id FROM daily_msg FINAL WHERE day = {0} AND week = {1}', [$day, $week])->fetchOne('msg_id');
        $msg = [
            'chat_id' => env('DAILY_TASK_CHANNEL'),
            'text' => $msg,
            'parse_mode' => 'markdown',
            'disable_notification' => true
        ];
        if(is_numeric($last_post)){
            Request::editMessageText(array_merge($msg, ['message_id' => $last_post]));
        }else{
            $msg_id = Request::sendMessage($msg)->getResult()->message_id;
            self::getDB()->insert('daily_msg', [[$day, $week, $msg_id]], ['day', 'week', 'msg_id']);
        }

    }
    private function genText(array $data, $small = false){
        $msg = "";
        //           $msg .= "#lesson"+$data['week'].PHP_EOL;
        if ($small == false){
            $msg .= 'ДЗ на '.self::getDayByDayAndWeek($data['day']-1, $data['week'])." ";
            $msg .= "(".Week::getDayString($data['day']).")".PHP_EOL;
        }
        $msg .= "№*".$data['num'].'* '.$data['name'].": ";
        $msg .= '`'.$data['task'];
        if($small == false && $data['description'] != 'null') $msg .= '. '.$data['description'];
        $msg .= '`';
        return $msg;
    }

}