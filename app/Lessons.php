<?php
/**
 * User: alexey
 * Date: 30.08.18
 * Time: 14:59
 */

namespace App;


use App\Http\Controllers\Utils\DBConnect;

class Lessons {
    use DBConnect;
    public static $basic = "SELECT day, num, text FROM lessons FINAL ANY LEFT JOIN (SELECT * FROM lesson_ids FINAL) USING id ORDER BY day, num";
    public static function getAll(){
        return self::getDB()->select(self::$basic)->rawData()['data'];
    }
    public static function getLessons(){
        return self::getDB()->select('SELECT id, text FROM lesson_ids FINAL')->rawData()['data'];
    }
    public static function getSchedule($day = null){
        $base = "SELECT day, num, id as lesson_id FROM lessons FINAL ";
        if(is_numeric($day)) $base .='WHERE day = '.$day;
        return self::getDB()->select($base)->rawData()['data'];
    }
    public static function getLesson($day, $num){
        return self::getDB()->select("SELECT day, num, id as lesson_id FROM lessons FINAL WHERE day = {day} AND num = {num}", ['day' => $day, 'num' => $num])->fetchOne();
    }
    public static function getLessonsById($id){
    public static function findNextLesson(int $day, int $num): array {
        $id = self::getLesson((int)$day, (int)$num)['lesson_id'];
        $lessons = self::getDB()->select('SELECT day, num, id as lesson_id FROM lessons FINAL WHERE id ='.$id.' ORDER BY day ASC')->rawData()['data'];
        $dt = Carbon::now(new \DateTimeZone('Europe/Kiev'));

        $result = [];
        foreach ($lessons as $lesson){ //поиск на этой неделе
            if(empty($result)){
                if($lesson['day'] > $dt->dayOfWeekIso){
                    $dt->addDays($lesson['day'] - $dt->dayOfWeekIso);
                    $result = $lesson;
                }
            }
        }
        if(empty($result)){// поиск на следующей неделе
            foreach ($lessons as $lesson){
                if(empty($result) && ($lesson['day'] < $dt->dayOfWeekIso)){
                    $dt->addWeek();
                    if($dt->dayOfWeekIso  > $lesson['day']){
                        $dt->day = $dt->day - ($dt->dayOfWeekIso-$lesson['day']);
                    }else{ //кажеться это лишние
                        $dt->day = $dt->day + ($lesson['day'] - $dt->dayOfWeekIso);
                    }

                    $result = $lesson;
                }
            }
        }

        if(empty($result) && count($lessons) == 1){ //если один урок в неделю
            $result = $lessons[0];
            $dt->addWeek();
        }

        $result['date'] = $dt->format("Y-m-d");
        $result['timestamp'] = $dt->getTimestamp();
        return $result;
    }
}