<?php
/**
 * User: alexey
 * Date: 22.08.18
 * Time: 20:42
 */

namespace App;

use App\Http\Controllers\Utils\DBConnect;

class Task {
    use DBConnect;
    static $basic = "SELECT task_id, week, day, lesson_num as num, text as name, task, description, id as lid FROM tasks FINAL
          ANY LEFT JOIN (
              SELECT day, num, text, id FROM lessons FINAL ANY LEFT JOIN (SELECT * FROM lesson_ids FINAL) USING id
          ) USING day, num ";
    public static function getTaskById(int $task_id){
        return self::getDB()->select(self::$basic.'WHERE task_id = '.$task_id)->fetchOne();
    }
    public static function getTaskByNameAndDate(string $name, int $week, int $day){
        return self::getDB()->select(self::$basic.'WHERE name = \'{0}\' AND week = {1} AND day = {2}', [$name, $week, $day])->fetchOne();
    }
    public static function getAllByWeek($week = null){
        if(!is_numeric($week)) $week = date('W');
        return self::getDB()->select(self::$basic.'WHERE week = '.$week. ' ORDER BY num')->rawData()['data'];
    }
    public static function getDayByWeekAndDay(int &$week = -1, int &$day = -1){
        if($week === -1) $week = date('W');
        if($day === -1) $day = date('w'); //deyOfWeek
        return self::_getDayByWeekAndDay($week, $day);
    }
    public static function _getDayByWeekAndDay(int $week, int $day){
        return self::getDB()->select(self::$basic.'WHERE week = '.$week.' AND day = '.$day.' ORDER BY num')->rawData()['data'];
    }
    /* необходимо для синхронизации */
    public static function getAll(): array {
        return self::getDB()->select(self::$basic.'WHERE week >= '.date('W'))->rawData()['data'];
    }

}