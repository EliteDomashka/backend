<?php

namespace App\Console\Commands;

use App\Http\Controllers\Utils\DBConnect;
use Illuminate\Console\Command;

class CHImportScheme extends Command {
    use DBConnect;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ch:scheme';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Database scheme';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Starting...');
        $scheme = <<<EOD
CREATE TABLE IF NOT EXISTS daily_msg (
  day UInt8,
  week UInt8,
  msg_id UInt16, 
  year UInt16 DEFAULT toYear(updated_date),
  updated_date Date DEFAULT now()
) ENGINE = ReplacingMergeTree(updated_date, (day, week), 8192);
CREATE TABLE IF NOT EXISTS notification (
  id UInt32,
  task_id UInt16,
  action UInt8, 
  updated_date Date DEFAULT now()
) ENGINE = ReplacingMergeTree(updated_date, (id, task_id), 8192);
CREATE TABLE IF NOT EXISTS gdz (
  lesson_id UInt8,
  task Array(String),
  img String,
 updated_date Date DEFAULT now()
) ENGINE = ReplacingMergeTree(updated_date, (lesson_id, task), 8192);
CREATE TABLE IF NOT EXISTS lesson_ids (
  id UInt8,
  text String,
  updated_date Date DEFAULT now()
) ENGINE = ReplacingMergeTree(updated_date, id, 8192);
CREATE TABLE IF NOT EXISTS lessons (
  day UInt8,
  num UInt8,
  id UInt8,
  updated_date Date DEFAULT now(),
  version Int8 DEFAULT 1
) ENGINE = CollapsingMergeTree(updated_date, (day, num), 8192, version);
CREATE TABLE IF NOT EXISTS tasks (
  task_id UInt16,
  day UInt8,
  lesson_num UInt8,
  publisher_id UInt16 DEFAULT 1,
  task String,
  description String DEFAULT 'null',
  week UInt8,
  updated_date Date DEFAULT today(),
  version Int8 DEFAULT 1
) ENGINE = CollapsingMergeTree(updated_date, task_id, 8192, version);
CREATE TABLE IF NOT EXISTS tg_msg (task_id UInt16, msg_id UInt16) ENGINE = TinyLog;
EOD;
        $exp = explode(';', $scheme);
        foreach ($exp as $query){
            if($query != "") self::getDB()->write($query);
        }
        $this->info('OK');
    }
}
