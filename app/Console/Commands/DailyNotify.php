<?php

namespace App\Console\Commands;

use App\Http\Controllers\Notify\Telegram;
use App\Task;
use Illuminate\Console\Command;

class DailyNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $week = -1;
        $day = date('w');
        if($day == 5){
            $week = date('W')+1;
            $day = 0;
        }
        if($day > 5) return;
        $day++;
        $data = Task::getDayByWeekAndDay($week, $day);

        foreach ([Telegram::class] as $class){
            (new $class)->dailySend($data, $week, $day);
        }
    }
}
