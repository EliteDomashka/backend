<?php
/**
 * User: alexey
 * Date: 30.10.18
 * Time: 17:20
 */

namespace Longman\TelegramBot\Commands\SystemCommands;


use App\Http\Controllers\Utils\DBConnect;
use App\Lessons\Finder;
use App\Lessons\Lesson;
use App\Telegram\Menu;
use App\TextParser;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Conversation;

class GdzResultCommand extends SystemCommand {
    use DBConnect;

    protected $name = 'gdzresult';
    protected $description = 'fdggsddf';
    protected $usage = '/start';
    protected $version = '1.0.0';

    public function execute() {
        $conversation = new Conversation(($chat_id = $this->getMessage()->getChat()->getId()), ($user_id = $this->getMessage()->getFrom()->getId()), 'gdzresult');
        if(!$conversation->exists()) return Request::emptyResponse();
        $result = Finder::parse($lesson_id = $conversation->notes['lesson_id'], $this->getMessage()->getText());
        $text = "";
        $senddata = [
            'chat_id' => $chat_id,
        ];
//        Log::warning($msg);
//        Log::warning(Lesson::getById($lesson_id)->templates);
//        Log::warning($result);
        if(is_array($result)){
            try{
                $url = Finder::getURL($lesson_id, $result);
                if(!is_null($url)){
                    $senddata['photo'] = $url;
                    $senddata['caption'] = 'Данні взятi з 4book.org';
                    Request::sendPhoto($senddata);
                    return Request::sendMessage([
                        'chat_id' => $chat_id,
                        'text' => "Ви можете отримати йще ГДЗ по цьомоу придмету, використовуючі інструкцію яка зазаначена раніше. Для того щоб дізнатися для іншого придмету, натисніть:",
                        'reply_markup' => Menu::getMenu(2)
                    ]);
                }
            }catch (\Exception $exp){
                Log::error($exp);
            }
            $text = 'Помилка отримання даних, мабудь цієї вправи намає';
        }else{
            $text = "Щось пішло не так...";
        }
        $senddata['text'] = $text;
        return Request::sendMessage($senddata);
    }
}