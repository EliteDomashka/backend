<?php
/**
 * User: alexey
 * Date: 30.10.18
 * Time: 16:51
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use App\Lessons\Finder;
use App\Lessons\Lesson;
use App\Telegram\Menu;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;

class CallbackqueryCommand extends SystemCommand {

    protected $name = 'callbackquery';
    protected $description = 'Reply to callback query';
    protected $version = '1.1.1';

    public function execute() {
        $callback_query = $this->getCallbackQuery();
        $callback_data = $callback_query->getData();
        $callback_data = explode('_', $callback_data);

        $conversation = new Conversation(($chat_id = $callback_query->getMessage()->getChat()->getId()), ($user_id = $callback_query->getFrom()->getId()), 'gdzresult');
        $notes = &$conversation->notes;
        $answer = "Додано в обробку";
        $senddata = [
            'chat_id' => $chat_id,
            'message_id' => ($message_id = $callback_query->getMessage()->getMessageId()),
            'parse_mode' => 'markdown',
        ];

        switch ($callback_data[0]){
            case 'choselesson':
                if(@is_numeric($callback_data[1])){
                    $notes['lesson_id'] = $callback_data[1];
                    $notes['msg_id'] = $message_id;
                    $senddata['text'] = 'Добре, тепер напишіть номер (або разом с стр) у подібному форматі: `'.Finder::getById($notes['lesson_id'])->example.'`';
                }else{
                    $conversation->stop();
                    $senddata['text'] = 'Оберіть придмет';
                    $senddata['reply_markup'] = Menu::getMenu(1);
                }
                break;
        }
        Request::editMessageText($senddata);
        $conversation->update();
//        $data = ['callback_query_id' => $callback_query_id, $result, 'cache_time' => 5,];
//        return Request::answerCallbackQuery($data);
    }
}