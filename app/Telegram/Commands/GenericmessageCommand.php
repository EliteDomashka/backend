<?php
/**
 * User: alexey
 * Date: 30.10.18
 * Time: 17:05
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;

class GenericmessageCommand extends SystemCommand {

    protected $name = 'genericmessage';
    protected $description = 'Handle generic message';
    protected $version = '1.1.0';
    protected $need_mysql = true;

    public function executeNoDb() {
        return Request::emptyResponse();
    }

    public function execute() {
        $conversation = new Conversation($this->getMessage()->getFrom()->getId(), $this->getMessage()->getChat()->getId());
        if ($conversation->exists() && ($command = $conversation->getCommand())) {
            return $this->telegram->executeCommand($command);
        }
        return Request::emptyResponse();
    }
}