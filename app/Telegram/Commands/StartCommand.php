<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use App\Telegram\Menu;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;

class StartCommand extends SystemCommand {
    protected $name = 'start';                      // Your command's name
    protected $description = 'Початок'; // Your command description
    protected $usage = '/start';                    // Usage of your command
    protected $version = '1.0.0';                  // Version of your command
    protected $private_only = true;

    public function execute() {
        $message = $this->getMessage();

        $chat_id = $message->getChat()->getId();

        $data = [
            'chat_id' => $chat_id,
            'text'    => 'Привіт! Вибери предмет по якому хочеш отримати відповідь',
            'reply_markup' => Menu::getMenu(1)
        ];

        return Request::sendMessage($data);
    }
}