<?php
/**
 * User: alexey
 * Date: 29.10.18
 * Time: 17:25
 */

namespace App\Telegram;


use App\Http\Controllers\Utils\DBConnect;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\InlineKeyboardButton;

class Menu {
    use DBConnect;

    public static function getMenu(int $menu) {
        $inline_keyboard = null;
        switch ($menu) {
            case 1:
                $lessons = self::lessonList();
                $result = [];
                foreach ($lessons as $lesson){
                    $result[] = new InlineKeyboardButton([
                        'text' => $lesson['name'],
                        'callback_data' => 'choselesson_'.$lesson['id']
                    ]);
                }
                $inline_keyboard = $result;
                break;
            case 2:
                $inline_keyboard = [
                    new InlineKeyboardButton([
                        'text' => '⬅ Назад️',
                        'callback_data' => 'choselesson'
                    ])
                ];
                break;
        }
        return new InlineKeyboard(...$inline_keyboard);
    }
    public static function lessonList(){
        return self::getDB()->select("SELECT id, text as name FROM lesson_ids ANY INNER JOIN (SELECT lesson_id as id FROM gdz GROUP BY lesson_id) USING id ")->rawData()['data'];
    }
}